# Installation
> `npm install --save @types/create-react-class`

# Summary
This package contains type definitions for create-react-class (https://facebook.github.io/react/).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/create-react-class

Additional Details
 * Last updated: Thu, 17 Oct 2019 21:27:05 GMT
 * Dependencies: @types/react
 * Global values: createReactClass

# Credits
These definitions were written by John Gozde <https://github.com/jgoz>.
